import os
import io
from nltk.corpus import wordnet as wn
import pickle
import time
import numpy as np

def loadDataset(name):
    """
    open pickles files

    :param name: name of the pickle
    :return data: variable containing the data of the pickle
    """

    infile = open(name,'rb')
    dataset = pickle.load(infile)
    infile.close()
    return dataset


def loadMapping(gold, train):
    """
    read the file containing the mapping between token words and associated senses in different formats

    :param gold: the mapping file.
    :param train: flag, false when the gold referss to the eval datasets and enble saving of pickles
    :return map_key_to_wn: map dictionary between keys and wordnet synset
    :map_key_to_lex: map dictionary between keys and lexicographers ID
    """
    map_key_to_wn = dict()
    with io.open(gold, "r", encoding="utf-8") as gold:
        for line in gold:
            pair=line.split()
            if len(pair)==2:
                synset = wn.lemma_from_key(str(pair[1]))
                synset_id = "wn:" + str(synset.synset().offset()).zfill( 8) + synset.synset().pos()
                map_key_to_wn[pair[0]]=[synset_id]
            else:
                map_key_to_wn[pair[0]]=[]
                for i in range(1, len(pair)):
                    synset = wn.lemma_from_key(str(pair[i]))
                    synset_id = "wn:" + str(synset.synset().offset()).zfill( 8) + synset.synset().pos()
                    map_key_to_wn[pair[0]].append(synset_id)

    map_wn_to_bn = dict()
    with io.open("../resources/babelnet2wordnet.tsv", "r", encoding="utf-8") as b2w:
        for line in b2w:
            pair=line.split()
            map_wn_to_bn[pair[1]]=pair[0]

    map_bn_to_lex = dict()
    with io.open("../resources/babelnet2lexnames.tsv", "r", encoding="utf-8") as w2l:
        for line in w2l:
            pair=line.split()
            map_bn_to_lex[pair[0]]=pair[1]



    map_wn_to_lex=dict()
    for wnet in map_wn_to_bn:
        map_wn_to_lex[wnet]=map_bn_to_lex[map_wn_to_bn[wnet]]


    map_key_to_lex=dict()
    for key in map_key_to_wn:
        map_key_to_lex[key]=[]
        for item in map_key_to_wn[key]:
            if item in map_wn_to_lex:
                map_key_to_lex[key].append(map_wn_to_lex[item])
            else: print("no w", item)

    if train==False:
        pic_map = open("wn2lex.pickle",'wb')
        pickle.dump(map_wn_to_lex, pic_map)
        pic_map.close()

        pic_map1 = open("key2wn_07.pickle",'wb')
        pickle.dump(map_key_to_wn, pic_map1)
        pic_map1.close()

        pic_map2 = open("key2lex_07.pickle",'wb')
        pickle.dump(map_key_to_lex, pic_map2)
        pic_map2.close()



    return map_key_to_wn, map_key_to_lex

def createVocabulary(dataset, name):
    """
    create a word-to-ID (and ID-to-word) dictionary for the enconding of the sets.


    :param dataset: the original dataset.
    :param name: name that will be given to the dictionary
    :return word_to_id: word_to_id dictionary
    """
    pic_vocab = open(name+".pickle",'wb')
    pic_vocab_opp = open(name+"_opp.pickle",'wb')
    word_to_id = dict()
    id_to_word= dict()
    word_to_id["<PAD>"] = 0
    word_to_id["<UNK>"] = 1
    id_to_word[0]="<PAD>"
    id_to_word[1]="<UNK>"
    i=len(word_to_id)
    for line in dataset:
        for c in line:
            if (c not in word_to_id and c!='\n'):
                word_to_id[c] = i
                id_to_word[i]=c
                i+=1


    pickle.dump(word_to_id, pic_vocab)
    pickle.dump(id_to_word, pic_vocab_opp)         
    pic_vocab.close()
    pic_vocab_opp.close()

    return word_to_id



def encodeClassList(set_vocab, labelF_vocab, labelC_vocab):
    """
    encode into IDs the file containing the senses associated to each word. 
    In the "elmo_encoded" versions only the sesnes are encoded, not the word.


    :param set_vocab: word_to_id dictionary of the dataset.
    :param labelF_vocab: word_to_id dictionary of the labels for fine-grained wsd
    :param labelF_vocab: word_to_id dictionary of the labels for coarse-grained wsd
    """
    classF_dict=loadDataset("classF_list.pickle")
    classC_dict=loadDataset("classC_list.pickle")
    encodedF_class_dict=dict()
    encodedC_class_dict=dict()

    elmo_encodedF_class_dict=dict()
    elmo_encodedC_class_dict=dict()

    for line in classF_dict:
        encoded_senseF=[]
        for sense in classF_dict[line]:
            encoded_senseF.append(labelF_vocab[sense])
        id_word=set_vocab[line]
        encodedF_class_dict[id_word]=encoded_senseF
        elmo_encodedF_class_dict[line]=encoded_senseF

    for line in classC_dict:
        encoded_senseC=[]
        for sense in classC_dict[line]:
            encoded_senseC.append(labelC_vocab[sense])
        id_word=set_vocab[line]
        encodedC_class_dict[id_word]=encoded_senseC
        elmo_encodedC_class_dict[line]=encoded_senseC

    pic_classF = open("en_classF_list.pickle",'wb')
    pic_classC = open("en_classC_list.pickle",'wb')
    pickle.dump(encodedF_class_dict, pic_classF)   
    pickle.dump(encodedC_class_dict, pic_classC)       
    pic_classF.close()
    pic_classC.close()

    pic_elmo_classF = open("elmo_en_classF_list.pickle",'wb')
    pic_elmo_classC = open("elmo_en_classC_list.pickle",'wb')
    pickle.dump(elmo_encodedF_class_dict, pic_elmo_classF)   
    pickle.dump(elmo_encodedC_class_dict, pic_elmo_classC)       
    pic_elmo_classF.close()
    pic_elmo_classC.close()


def encodeDataset(dataset, vocab, name):
    """
    encode into IDs a dataset (input or labels)


    :param dataset: original dataset.
    :param vocab: word-to-id dictionary
    :param name: name that will be given to the pickle file
    :return new_dataset: encoded dataset.
    """
    new_dataset=[]
    for line in dataset:
        new_line=[]
        for word in line:
            if word in vocab:
                new_line.append(vocab[word])
            else: new_line.append(1)
        new_dataset.append(new_line)

    if name != 'None':
        pic_data = open(name+".pickle",'wb')
        pickle.dump(new_dataset, pic_data)        
        pic_data.close()

    return new_dataset


# def storeCoordinates(n_sentence, maxsize):
#     pic_c = open("coordinates.pickle",'wb')
#     coordinate=np.zeros((n_sentence, maxsize, 2))
#     for l in range(n_sentence):
#         for i in range(maxsize):
#             coordinate[l][i]=[l,i]


#     pickle.dump(coordinate, pic_c)        
#     pic_c.close()


def resizeBatch(data, l):
    """
    resize the input data with a maximum lenght for each sentences. Longer sentences will be splitted and
    considered as 2 (or more)

    :param data: original input
    :param l: maximum length admitted
    :return new_batch: resized data
    """
    new_batch=[]
    for line in data:
        if (len(line)<l):
            new_batch.append(line)
        else:
            while(len(line)>l):
                new_batch.append(line[:l])
                line=line[l:]
            new_batch.append(line)
    return new_batch