import os
import io
import xml.etree.ElementTree as etree1
import lxml.etree as etree
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords
import string
import pickle
from utils import *
punctuation2=["``",  "''"]


def parsing(mapF, mapC, source_xml, train):
    """
    Parse the XML file.


    :param mapF: mapping for the synset associated to the world for fine-grained wsd
    :param mapC: mapping for the synset associated to the world for coarse-grained wsd
    :param source_xml: original XML file
    :param train: flag to distinguish if it's a train or a eval file
    :return data_set: dataset
    :return labelF_set: labels for fine-grained wsd
    :return labelC_set: labels for coarse-grained wsd
    """
    
    data_dict_tmp=dict()
    labelF_dict_tmp=dict()
    labelC_dict_tmp=dict()
    vocabF_class=dict()
    vocabC_class=dict()
    for event, elem in etree.iterparse(source_xml, events=('start', 'end'), encoding="utf-8"):
        if(event == 'start'):
            if elem.tag == "sentence":
                id_sen=elem.attrib["id"]
                new_line=[]
                if id_sen not in data_dict_tmp:
                    data_dict_tmp[id_sen]=[]
                    labelF_dict_tmp[id_sen]=[]
                    labelC_dict_tmp[id_sen]=[]

            elif elem.tag == "wf":
                word=str(elem.text).lower()
                #uncomment following line to remove punctuation and stopwords
                #if (word not in string.punctuation and word not in punctuation2 and word not in stopwords.words('english')):
                word=word.replace(" ", "_").replace("-", "_")
                data_dict_tmp[id_sen].append(word)
                labelF_dict_tmp[id_sen].append(word)
                labelC_dict_tmp[id_sen].append(word)


            elif elem.tag == "instance":
                word=str(elem.text).lower()
                #uncomment following line to remove punctuation and stopwords
                #if (word not in string.punctuation and word not in punctuation2 and word not in stopwords.words('english')):
                word=word.replace(" ", "_").replace("-", "_")
                id_syn=str(elem.attrib["id"])
                senseF=mapF[id_syn]
                senseC=mapC[id_syn]
                data_dict_tmp[id_sen].append(word)
                labelF_dict_tmp[id_sen].append(str(senseF[0]))
                labelC_dict_tmp[id_sen].append(str(senseC[0]))

                if word not in vocabC_class:
                    vocabF_class[word]= [senseF[0]]
                    vocabC_class[word]= [senseC[0]]
                else:
                    sense_setF=set(vocabF_class[word])
                    sense_setF.add(senseF[0])
                    vocabF_class[word]=sense_setF

                    sense_setC=set(vocabC_class[word])
                    sense_setC.add(senseC[0])
                    vocabC_class[word]=sense_setC
                        

        elem.clear()

    # remove empty sentences
    data_dict=dict()
    labelF_dict=dict()
    labelC_dict=dict()
    for key in data_dict_tmp:
        if len(data_dict_tmp[key])>0:
            data_dict[key]=data_dict_tmp[key]
            labelF_dict[key]=labelF_dict_tmp[key]
            labelC_dict[key]=labelC_dict_tmp[key]


    data_set=list(data_dict.values())
    labelF_set =list(labelF_dict.values())
    labelC_set =list(labelC_dict.values())

    if train:
        pic_data = open("train_data.pickle",'wb')
        pic_labelF = open("train_labelF.pickle",'wb')
        pic_labelC = open("train_labelC.pickle",'wb')
        pic_classF = open("classF_list.pickle",'wb')
        pic_classC = open("classC_list.pickle",'wb')
        pickle.dump(vocabF_class, pic_classF)
        pickle.dump(vocabC_class, pic_classC)
        pic_classF.close()
        pic_classC.close()

    else:
        pic_data = open("dev_data.pickle",'wb')
        pic_labelF = open("dev_labelF.pickle",'wb')
        pic_labelC = open("dev_labelC.pickle",'wb')



    pickle.dump(data_set, pic_data)
    pickle.dump(labelF_set, pic_labelF)
    pickle.dump(labelC_set, pic_labelC)
            
    pic_data.close()
    pic_labelF.close()
    pic_labelC.close()
    

    return data_set, labelF_set, labelC_set


def parsing_for_predict(source_xml):
    """
    Parse the XML file for the Prediction.py.

    :param source_xml: original XML file
    :return data_set: dataset
    :return labelF_set: labels for fine-grained wsd
    :return labelC_set: labels for coarse-grained wsd
    :return index_list: list of the index of each sense
    """

    data_dict_tmp=dict()
    labelF_dict_tmp=dict()
    labelC_dict_tmp=dict()
    index_list=[]

    for event, elem in etree.iterparse(source_xml, events=('start', 'end'), encoding="utf-8"):
        if(event == 'start'):
            if elem.tag == "sentence":
                id_sen=elem.attrib["id"]
                if id_sen not in data_dict_tmp:
                    data_dict_tmp[id_sen]=[]
                    labelF_dict_tmp[id_sen]=[]
                    labelC_dict_tmp[id_sen]=[]

            elif elem.tag == "wf":
                word=str(elem.text).lower()
                word=word.replace(" ", "_").replace("-", "_")
                data_dict_tmp[id_sen].append(word)
                labelF_dict_tmp[id_sen].append(word)
                labelC_dict_tmp[id_sen].append(word)
                index_list.append(0)

            elif elem.tag == "instance":
                word=str(elem.text).lower()
                word=word.replace(" ", "_").replace("-", "_")
                id_syn=str(elem.attrib["id"])
                data_dict_tmp[id_sen].append(word)   
                index_list.append(id_syn)
                labelF_dict_tmp[id_sen].append('wn:0') #placeholder
                labelC_dict_tmp[id_sen].append('noun.sense')    #placeholder                 

        elem.clear()

    data_set=list(data_dict_tmp.values())
    labelF_set =list(labelF_dict_tmp.values())
    labelC_set =list(labelC_dict_tmp.values())
  
    return data_set, labelF_set, labelC_set, index_list



if __name__ == '__main__':

    #Pre-process for Train set
    mapTrainF, mapTrainC = loadMapping("../../WSD_Evaluation_Framework/Training_Corpora/SemCor/semcor.gold.key.txt", True)
    train_data_set, train_labelF_set, train_labelC_set = parsing(mapTrainF, mapTrainC, "../../WSD_Evaluation_Framework/Training_Corpora/SemCor/semcor.data.xml", True)

    data_vocab = createVocabulary(train_data_set, "data_vocab")
    labelF_vocab = createVocabulary(train_labelF_set, "labelF_vocab")
    labelC_vocab = createVocabulary(train_labelC_set, "labelC_vocab")

    _ = encodeDataset(train_data_set, data_vocab, "train_set_encoded")
    _ = encodeDataset(train_labelF_set, labelF_vocab, "train_labelF_encoded")
    _ = encodeDataset(train_labelC_set, labelC_vocab, "train_labelC_encoded")

    encodeClassList(data_vocab, labelF_vocab, labelC_vocab)


    # #Pre-process for Dev set
    mapDevF, mapDevC = loadMapping("../../WSD_Evaluation_Framework/Evaluation_Datasets/semeval2007/semeval2007.gold.key.txt", False)
    dev_data_set, dev_labelF_set, dev_labelC_set = parsing(mapDevF, mapDevC, "../../WSD_Evaluation_Framework/Evaluation_Datasets/semeval2007/semeval2007.data.xml", False)

    _ = encodeDataset(dev_data_set, data_vocab, "dev_set_encoded")
    _ = encodeDataset(dev_labelF_set, labelF_vocab, "dev_labelF_encoded")
    _ = encodeDataset(dev_labelC_set, labelC_vocab, "dev_labelC_encoded")





        
